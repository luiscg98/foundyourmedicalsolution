package mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui.consultingRoom.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.R
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.databinding.ItemNoteBinding

class ConsultingRoomAdapter(private val consultingRooms:List<ConsultingRoom>, private val listener:(ConsultingRoom) -> Unit) :
 RecyclerView.Adapter<ConsultingRoomAdapter.ViewHolder>(){

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){
        val binding = ItemNoteBinding.bind(v)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val consultingRoom = consultingRooms[position]
        holder.binding.textTitle.text = consultingRoom.name.toString()
        holder.binding.textDescription.text = consultingRoom.description.toString()
        holder.itemView.setOnClickListener{
            listener(consultingRoom)
        }
        Picasso.get().load(consultingRoom.image).fit().centerCrop().placeholder(R.drawable.no_image_found).into(holder.binding.imgNota)
    }

    override fun getItemCount(): Int = consultingRooms.size
}
