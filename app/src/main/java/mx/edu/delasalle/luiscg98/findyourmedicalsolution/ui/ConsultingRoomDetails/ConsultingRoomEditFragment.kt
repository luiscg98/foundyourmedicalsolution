package mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui.ConsultingRoomDetails

import android.os.Bundle
import android.os.Message
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.RemoteMessage
import com.squareup.picasso.Picasso
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.R
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.core.Resource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local.AppDatabase
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local.LocalDataSource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote.ApiClient
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote.ConsultingRoomDataSource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.databinding.FragmentConsultingRoomEditBinding
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.presentation.ConsultingRoomViewModel
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.presentation.ConsultingRoomViewModelFactory
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.repository.ConsultingRoomRepositoryImp


class ConsultingRoomEditFragment : Fragment(R.layout.fragment_consulting_room_edit) {

    private lateinit var binding: FragmentConsultingRoomEditBinding

    private val viewModel by viewModels<ConsultingRoomViewModel> {
        ConsultingRoomViewModelFactory(ConsultingRoomRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).consultingRoomDao()),
            ConsultingRoomDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentConsultingRoomEditBinding.bind(view)

        binding.editImageUrl.addTextChangedListener(object: TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                //Toast.makeText(requireContext(), "${s.toString()}", Toast.LENGTH_LONG).show()
                Picasso.get().load("${s}").fit().centerCrop().placeholder(R.drawable.no_image_found).into(binding.imageView2)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Toast.makeText(requireContext(), "beforeTextChanged", Toast.LENGTH_LONG).show()
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //Toast.makeText(requireContext(), "onTextChanged", Toast.LENGTH_LONG).show()
            }
        })

        binding.btnAddNote.setOnClickListener {

            var consultingRoom = ConsultingRoom(
                "",
                binding.editTitle.text.toString(),
                binding.editContact.text.toString(),
                binding.editAddress.text.toString(),
                binding.editContent.text.toString(),
                binding.editImageUrl.text.toString()
            )

            viewModel.saveConsultingRoom(consultingRoom).observe(viewLifecycleOwner, Observer { result ->
                when(result){
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        findNavController().popBackStack()
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "${result.exception.toString()}", Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }
}