package mx.edu.delasalle.luiscg98.findyourmedicalsolution.repository

import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local.LocalDataSource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomList
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.toConsultingRoomEntity
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote.ConsultingRoomDataSource

class ConsultingRoomRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: ConsultingRoomDataSource
    ) : ConsultingRoomRepository {
    override suspend fun getConsultingRooms(): ConsultingRoomList {

        dataSource.getConsultingRooms().data.forEach{ consultingRoom ->
            localDataSource.saveConsultingRoom(consultingRoom.toConsultingRoomEntity())
        }

        return localDataSource.getConsultingRooms()
    }

    override suspend fun saveConsultingRoom(consultingRoom: ConsultingRoom?): ConsultingRoom? {
        return dataSource.saveConsultingRoom(consultingRoom)
    }

}