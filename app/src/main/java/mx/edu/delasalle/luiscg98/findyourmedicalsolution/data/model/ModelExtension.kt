package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model

fun List<ConsultingRoomEntity>.toConsultingRoomList(): ConsultingRoomList {
    val list = mutableListOf<ConsultingRoom>()

    this.forEach{ consultingRoomEntity ->
        list.add(consultingRoomEntity.toConsultingRoom())
    }

    return ConsultingRoomList(list)
}

fun ConsultingRoom.toConsultingRoomEntity(): ConsultingRoomEntity = ConsultingRoomEntity(
    this.id,
    this.name,
    this.contact,
    this.address,
    this.description,
    this.image
)

fun ConsultingRoomEntity.toConsultingRoom():ConsultingRoom = ConsultingRoom(
    this.id,
    this.name,
    this.contact,
    this.address,
    this.description,
    this.image
)