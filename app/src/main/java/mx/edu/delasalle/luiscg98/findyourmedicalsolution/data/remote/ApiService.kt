package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote

import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    //https://testapi.io/api/luiscg98/resource/consultingRooms
    @GET("consultingRoom/getAll")
    suspend fun getConsultingRooms(): ConsultingRoomList

    @POST("consultingRoom/save")
    suspend fun saveConsultingRoom(@Body consultingRoom:ConsultingRoom?) : ConsultingRoom?
}
