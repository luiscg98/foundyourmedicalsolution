package mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.R
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.commit
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.messaging.FirebaseMessaging
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui.consultingRoom.ConsultingRoomFragmentDirections

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navHostFragment.findNavController().run{
            toolbar.setupWithNavController(this, AppBarConfiguration(graph))
            supportActionBar?.title = "MedicalApp"
        }

        getNotificationId()

        FirebaseMessaging.getInstance().subscribeToTopic("android-device");
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.mymenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId)
        {
            R.id.item_1 -> {
                val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
                val action = ConsultingRoomFragmentDirections.actionConsultingRoomFragmentToAppDetailsFragment()
                navHostFragment.findNavController().navigate(action)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getNotificationId(){
        FirebaseMessaging.getInstance().token.addOnCompleteListener{ task ->
            val token = task.result

            Log.d("notes3", token!!)
        }
    }
}