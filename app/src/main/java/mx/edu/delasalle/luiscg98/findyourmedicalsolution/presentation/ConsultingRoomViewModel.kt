package mx.edu.delasalle.luiscg98.findyourmedicalsolution.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.core.Resource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.repository.ConsultingRoomRepository

class ConsultingRoomViewModel(private val repository: ConsultingRoomRepository): ViewModel() {

    fun fetchConsultingRooms() = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getConsultingRooms()))
        }catch (exception:Exception){
            emit(Resource.Failure(exception))
        }
    }

    fun saveConsultingRoom(consultingRoom: ConsultingRoom?) = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.saveConsultingRoom(consultingRoom)))
        }catch (exception:Exception){
            emit(Resource.Failure(exception))
        }
    }

}

class ConsultingRoomViewModelFactory(private val repository: ConsultingRoomRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ConsultingRoomRepository::class.java).newInstance(repository)
    }
}