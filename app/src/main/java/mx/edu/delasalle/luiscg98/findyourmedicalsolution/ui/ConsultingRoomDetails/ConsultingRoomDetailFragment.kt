package mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui.ConsultingRoomDetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.R
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.databinding.FragmentConsultingRoomDetailBinding

class ConsultingRoomDetailFragment : Fragment(R.layout.fragment_consulting_room_detail) {

    private lateinit var binding: FragmentConsultingRoomDetailBinding
    private val args by navArgs<ConsultingRoomDetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentConsultingRoomDetailBinding.bind(view)

        binding.textTitle.text = args.name
        binding.textContact.text = args.contact
        binding.textAddress.text = args.address
        binding.textContent.text = args.description
        Picasso.get().load(args.image).fit().centerCrop().placeholder(R.drawable.no_image_found).into(binding.imgNote)
    }
}