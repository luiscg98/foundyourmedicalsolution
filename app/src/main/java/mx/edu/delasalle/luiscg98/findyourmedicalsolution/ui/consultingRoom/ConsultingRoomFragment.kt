package mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui.consultingRoom

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.R
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.core.Resource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local.AppDatabase
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local.LocalDataSource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote.ApiClient
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote.ConsultingRoomDataSource
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.databinding.FragmentConsultingRoomBinding
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.presentation.ConsultingRoomViewModel
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.presentation.ConsultingRoomViewModelFactory
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.repository.ConsultingRoomRepositoryImp
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.ui.consultingRoom.adapters.ConsultingRoomAdapter

class ConsultingRoomFragment : Fragment(R.layout.fragment_consulting_room) {

    private lateinit var binding:FragmentConsultingRoomBinding
    private lateinit var adapter: ConsultingRoomAdapter

    private val viewModel by viewModels<ConsultingRoomViewModel> {
        ConsultingRoomViewModelFactory(ConsultingRoomRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).consultingRoomDao()),
            ConsultingRoomDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentConsultingRoomBinding.bind(view)

        binding.recyclerConsultingRooms.layoutManager = GridLayoutManager(requireContext(),1)

        binding.btnAddNote.setOnClickListener{
            val action = ConsultingRoomFragmentDirections.actionConsultingRoomFragmentToConsultingRoomEditFragment()
            findNavController().navigate(action)
        }

        binding.swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light);

        binding.swipeContainer.setOnRefreshListener {
            binding.recyclerConsultingRooms.adapter = null

            viewModel.fetchConsultingRooms().observe(viewLifecycleOwner, { result ->
                when(result){
                    is Resource.Loading -> {
                        //binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        //binding.progressbar.visibility = View.GONE

                        adapter = ConsultingRoomAdapter(result.data.data){ consultingRoom ->

                            onConsultingRoomClick(consultingRoom)
                        }

                        binding.recyclerConsultingRooms.adapter = adapter

                        Log.d("LiveData", "${result.data.toString()}")
                        binding.swipeContainer.setRefreshing(false)
                    }
                    is Resource.Failure -> {
                        binding.swipeContainer.setRefreshing(false)
                        Log.d("LiveData", "${result.exception.toString()}")
                    }
                }
            })

        }

        viewModel.fetchConsultingRooms().observe(viewLifecycleOwner, Observer { result ->

            when(result){
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE

                    adapter = ConsultingRoomAdapter(result.data.data){ consultingRoom ->

                        onConsultingRoomClick(consultingRoom)
                    }

                    binding.recyclerConsultingRooms.adapter = adapter

                    Log.d("LiveData", "${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData", "${result.exception.toString()}")
                }
            }
        })
    }

    private fun onConsultingRoomClick(consultingRoom:ConsultingRoom){
        val action = ConsultingRoomFragmentDirections.actionConsultingRoomFragmentToConsultingRoomDetailFragment(
            consultingRoom.id,
            consultingRoom.name,
            consultingRoom.contact,
            consultingRoom.address,
            consultingRoom.description,
            consultingRoom.image,
        )

        findNavController().navigate(action)
    }


}