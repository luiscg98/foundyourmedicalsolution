package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model

data class ConsultingRoomList (
    val data:List<ConsultingRoom> = listOf()
)