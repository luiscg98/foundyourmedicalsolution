package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomEntity

@Dao
interface ConsultingRoomDao {

    @Query("SELECT * FROM consultingRoomEntity")
    suspend fun getConsultingRooms(): List<ConsultingRoomEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveConsultingRoom(consultingRoom: ConsultingRoomEntity)
}