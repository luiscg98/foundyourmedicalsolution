package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomEntity

@Database(entities = [ConsultingRoomEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun consultingRoomDao(): ConsultingRoomDao

    companion object{

        private var INSTANCE: AppDatabase? = null

        fun getDataBase(context: Context) : AppDatabase {
            INSTANCE = INSTANCE?: Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "alzhera"
            ).build()

            return INSTANCE!!
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }
}