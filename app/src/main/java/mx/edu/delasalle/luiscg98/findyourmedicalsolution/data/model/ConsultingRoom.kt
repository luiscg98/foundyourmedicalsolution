package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model

data class ConsultingRoom (
    val id:String = "",
    val name:String = "",
    val contact:String = "",
    val address:String = "",
    val description:String = "",
    val image:String = ""
)