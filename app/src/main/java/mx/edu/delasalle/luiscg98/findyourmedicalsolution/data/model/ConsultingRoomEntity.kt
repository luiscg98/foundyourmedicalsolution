package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "consultingRoomEntity")
data class ConsultingRoomEntity (
    @PrimaryKey
    val id:String = "",
    @ColumnInfo(name="title")
    val name:String ="",
    @ColumnInfo(name="contact")
    val contact:String ="",
    @ColumnInfo(name="address")
    val address:String ="",
    @ColumnInfo(name="description")
    val description:String ="",
    @ColumnInfo(name="image")
    val image:String =""
)