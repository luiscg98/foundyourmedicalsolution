package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.local

import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.toConsultingRoomList
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomEntity
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomList

class LocalDataSource(private val consultingRoomDao: ConsultingRoomDao)
{
    suspend fun getConsultingRooms(): ConsultingRoomList = consultingRoomDao.getConsultingRooms().toConsultingRoomList()

    suspend fun saveConsultingRoom(consultingRoom: ConsultingRoomEntity) = consultingRoomDao.saveConsultingRoom(consultingRoom)
}