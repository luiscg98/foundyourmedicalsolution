package mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.remote

import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomList

class ConsultingRoomDataSource (private val apiService: ApiService) {
    suspend fun getConsultingRooms(): ConsultingRoomList = apiService.getConsultingRooms()

    suspend fun saveConsultingRoom(consultingRoom: ConsultingRoom?) : ConsultingRoom? = apiService.saveConsultingRoom(consultingRoom)
}