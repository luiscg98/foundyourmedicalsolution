package mx.edu.delasalle.luiscg98.findyourmedicalsolution.repository

import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoom
import mx.edu.delasalle.luiscg98.findyourmedicalsolution.data.model.ConsultingRoomList

interface ConsultingRoomRepository {
    suspend fun getConsultingRooms(): ConsultingRoomList
    suspend fun saveConsultingRoom(consultingRoom: ConsultingRoom?) : ConsultingRoom?
}